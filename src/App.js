import React from "react";
import './App.css';
import NavigationBar from "./navigation-bar";
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import MetamaskConnection from "./metamask-connection";
import AuctionContainer from "./auction/auction-container";
import Home from "./home";


function App() {

    return (
        <div>
            <Router>
                <div>
                    <NavigationBar/>
                    <Routes>
                        <Route
                            exact
                            path='/'
                            element={<Home/>}
                        />
                        <Route
                            exact
                            path='/auction'
                            element={<AuctionContainer/>}
                        />
                        <Route
                            exact
                            path='/metamask-account'
                            element={<MetamaskConnection/>}
                        />


                    </Routes>
                </div>
            </Router>

        </div>

    )

}

export default App;
