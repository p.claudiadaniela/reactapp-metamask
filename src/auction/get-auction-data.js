import {Row, Col, Label, Button, Input} from "reactstrap";
import web3, {auctionContractAddress} from '../web3';
import React, {useState} from "react";

const abi = require('../abi/AwesomePictureAuctions.json').abi;

function GetAuctionData(props) {
    const [auctionStartingPrice, setAuctionStartingPrice] = useState(["0"])
    const [auctionMaxBid, setAuctionMaxBid] = useState(["0"])

    const reloadData = ()=>{
        callGetPrice();
        callGetMaxBid();
    }
    const callGetPrice = () => {
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        auctionContract.methods.retrieveStartingPrice(props.auctionId).call().then((startingPrice) => {
            setAuctionStartingPrice([startingPrice])
        }).catch((err) => {
            alert(err)
        });
    }

    const callGetMaxBid = () => {
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        auctionContract.methods.retrieveMaxBid(props.auctionId).call().then((maxBid) => {
            setAuctionMaxBid([maxBid])
        }).catch((err) => {
            alert(err)
        });
    }

    return (
        <div>
            <Row>
                <Col sm={{size: '3', offset: 1}}>
                    <Button sm={{size: '3', offset: 1}} color="warning" onClick={reloadData}>Reload Data</Button>
                </Col>

                <Col>
                    <Label>Address : {auctionContractAddress}</Label>
                    <br/>
                    <Label>Auction Starting Price: {auctionStartingPrice}</Label>
                    <br/>
                    <Label>Auction Max Bid: {auctionMaxBid}</Label>
                </Col>
            </Row>
        </div>
    );
}

export default GetAuctionData;
