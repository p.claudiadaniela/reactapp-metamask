import React, {useEffect, useState} from "react";
import  {Badge,Modal, ModalBody, ModalHeader, Table} from "reactstrap";
import web3, {auctionContractAddress} from "../web3";
import BidForm from "./bid-form";

const abi = require('../abi/AwesomePictureAuctions.json').abi;
const textStyle = {
    color: 'black',
    textDecoration: 'none'
};


const AuctionsTables = (props) => {
    const [auctionId, setAuctionId] = useState(0);
    const [isSelected, setSelected] = useState(false);
    const [auctions, setAuctions] = useState([ ]);

    useEffect(() => {
        callGetAuction();
    }, []);


    const callGetAuction = () => {
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        setAuctions([]);
        let currentDate= new Date();
        for (let i = 1; i <= props.auctionIndex; i++) {
            auctionContract.methods.retrieveAuctionData(i).call().then((auction) => {
                auction["id"] = i;
                let sd = new Date(parseInt(auction[1]));
                let ed = new Date(parseInt(auction[2]));
                auction["startDate"] = sd.toLocaleDateString() ;
                auction["endDate"] = ed.toLocaleDateString() ;
                console.log(currentDate.getTime() < ed.getTime());
                auction["active"] = currentDate.getTime() < ed.getTime() ;

                setAuctions(auctions => auctions.concat(auction))
            }).catch((err) => {
                alert(err)
            });
        }
    }

    const reloadData = ()=>{
        toggleForm();
        callGetAuction();
    }

    function addBid(aId, active) {
        if(active) {
            setAuctionId(aId);
            toggleForm();
        }else{
            alert("Cannot add bid for inactive auction!");
        }

    }

    function toggleForm() {
        setSelected((isSelected) => (!isSelected));
    }
    return (
        <div>
            <Table hover>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Address</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Starting Price (WEI)</th>
                    <th>Max Bid (WEI)</th>
                    <th>Max Bidder</th>
                    <th>Active</th>
                </tr>
                </thead>
                <tbody>
                {auctions.length ?
                    auctions.map(d => (
                        <tr key={d.id} onClick={() =>  addBid(d.id, d.active)} title= {d.active? "Add Bid": "Cannot add bid!!"}>
                            <td>{d.id}</td>
                            <td>{d[0]}</td>
                            <td>{d.startDate}</td>
                            <td>{d.endDate}</td>
                            <td>{d[3]}</td>
                            <td>{d[4]}</td>
                            <td>{d[5]}</td>
                            <td>  {d.active &&   <Badge color="success"   style={{ color: 'green' }}> Active</Badge> }
                                    {!d.active &&   <Badge color="danger"  style={{ color: 'red' }}> Inactive</Badge> }
                            </td>
                        </tr>
                    ))
                    :
                    (<tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>)
                }
                </tbody>
            </Table>

            <Modal isOpen={isSelected} toggle={toggleForm} size="lg">
                <ModalHeader toggle={toggleForm}> Add Bid: </ModalHeader>
                <ModalBody>
                    <BidForm handler={reloadData} auctionId={auctionId}/>
                </ModalBody>
            </Modal>

        </div>
    );
};
export default AuctionsTables;