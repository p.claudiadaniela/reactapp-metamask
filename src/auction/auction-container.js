import React, {useState, useEffect } from "react";
import {Row,Button, Col, Modal, ModalHeader, ModalBody} from "reactstrap";
import BidForm from "./bid-form";
import AddAuction from "./add-auction";
import AuctionsTables from "./auctions-table";
import web3, {auctionContractAddress} from "../web3";
const abi = require('../abi/AwesomePictureAuctions.json').abi;

function AuctionContainer() {
    const [isSelected, setSelected] = useState(false);
    const [auctionsIndex, setAuctionsIndex] = useState(0);

    useEffect(() => {
        callGetAuction();
    }, []);

    const reloadData = ()=>{
        toggleForm();
        callGetAuction();
    }
    const callGetAuction = () => {
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        auctionContract.methods.retrieveAuctionsIndex().call().then((auctionId) => {
            setAuctionsIndex(auctionId)
        }).catch((err) => {
            alert(err)
        });
    }
    function toggleForm() {
        setSelected((isSelected) => (!isSelected));
    }

    return (
        <div style={{ padding: '50px' }}>
            <Row >
                <Col sm={{ size: '8', offset: 1 }} >
                    <Button color="warning" onClick={toggleForm}>Add Auction </Button>
                </Col>
            </Row>
            <Row >
                <Col sm={{ size: '8', offset: 1 }} >
                <AuctionsTables key={auctionsIndex} auctionIndex={auctionsIndex} />
                </Col>
            </Row>

            <Modal isOpen={isSelected} toggle={toggleForm} size="lg">
                <ModalHeader toggle={toggleForm}> Add Auction: </ModalHeader>
                <ModalBody>
                    <AddAuction handler={reloadData}/>
                </ModalBody>
            </Modal>

        </div>
    );
}

export default AuctionContainer;
