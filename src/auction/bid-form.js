import {Row, Col, Label, Button, Input} from "reactstrap";
import web3, {auctionContractAddress} from '../web3';
import React, {useState} from "react";

const abi = require('../abi/AwesomePictureAuctions.json').abi;

function BidForm(props) {
    const [bid, setBid] = useState({ "ethBidAmount": 0, "ethGasPrice": 0 });


    const sendBid = async () => {
        const accounts = await web3.eth.getAccounts();
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        auctionContract.methods.bid(props.auctionId).send({
            from: accounts[0],
            value: bid.ethBidAmount,
            gas: 200000,
            gasPrice: web3.utils.toWei(web3.utils.toBN(bid.ethGasPrice), 'gwei')
        }).on('error', (e) => {
            alert(e)
        }).on('transactionHash', (txHash) => {
            console.log(txHash);
        }).then((result) => {
            console.log(result);
            props.handler();
        }).catch((e) => {
            console.log(e);
        });

    };

    const onChange = (e) => {
        let prop = {};
        prop[e.target.name] = e.target.value;
        let bid1 = Object.assign({}, bid, prop);
        setBid(bid1);
        console.log(bid)

    };

    return (
        <div>

            <Row>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="ethBidAmount">Wei Bid Amount: </Label>
                    <Input type="number" name="ethBidAmount" size="sm" onChange={onChange}/>
                </Col>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="ethGasPrice">GWei Gas Price: </Label>
                    <Input type="number" name="ethGasPrice" size="sm" onChange={onChange}/>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col sm={{size: 'auto', offset: 1}}>
                    <Button color="danger" onClick={sendBid}>Submit</Button>
                </Col>
            </Row>
        </div>
    );
}

export default BidForm;
