import {Row, Col, Label, Button, Input} from "reactstrap";
import web3, {auctionContractAddress} from '../web3';
import React, {useState} from "react";

const abi = require('../abi/AwesomePictureAuctions.json').abi;

function AddAuction(props) {
    const [auction, setAuction] = useState({ "description": 0, "startingPrice": 0,
        "startTime": 0,"endTime": 0, "ethGasPrice": 0});


    const sendBid = async () => {
        const accounts = await web3.eth.getAccounts();
        const auctionContract = new web3.eth.Contract(abi, auctionContractAddress);
        auctionContract.methods.addAuction(auction.description, auction.startingPrice, auction.startTime, auction.endTime).send({
            from: accounts[0],
            gas: 200000,
            gasPrice: web3.utils.toWei(web3.utils.toBN(auction.ethGasPrice), 'gwei')
        }).on('error', (e) => {
            alert(e)
        }).on('transactionHash', (txHash) => {
            console.log(txHash);
        }).then((result) => {
            console.log(result);
            let event = result.events.AuctionCreated.returnValues;
            console.log(event);
            props.handler();
        }).catch((e) => {
            console.log(e);
        });

    };
    const onChange = (e) => {
        let prop = {};
        prop[e.target.name] = e.target.value;
        let auction1 = Object.assign({}, auction, prop);
        setAuction(auction1);
        console.log(auction)

    };

    return (
        <div>

            <Row>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="description">Description: </Label>
                    <Input type="text" name="description" size="sm" onChange={onChange}/>
                </Col>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="startingPrice">StartingPrice: </Label>
                    <Input type="number" name="startingPrice" size="sm" onChange={onChange}/>
                </Col>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="startTime">Start Time (millis): </Label>
                    <Input type="number" name="startTime" size="sm" onChange={onChange}/>
                </Col>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="endTime">End Time (millis): </Label>
                    <Input type="number" name="endTime" size="sm" onChange={onChange}/>
                </Col>
                <Col sm={{size: 4, offset: 1}}>
                    <Label for="ethGasPrice">GWei Gas Price: </Label>
                    <Input type="number" name="ethGasPrice" size="sm" onChange={onChange}/>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col sm={{size: 'auto', offset: 1}}>
                    <Button color="primary" onClick={sendBid}>Submit</Button>
                </Col>
            </Row>
        </div>
    );
}

export default AddAuction;
