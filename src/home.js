import React from 'react';
import { Button, Container, Jumbotron } from 'reactstrap';


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px"
};
const textStyle = { color: 'black', };

function Home() {
    return (
        <div style={{ padding: '50px' }}>
            <Jumbotron fluid style={backgroundStyle}>
                <Container fluid>
                    <h1 className="display-3" style={textStyle}>DApp Demo using React, OpenEthereum, Solidity, Metamask</h1>
                    <hr className="my-2" />
                    <p className="lead" style={textStyle}> This project has been developed according to the guide book :
                       <b> Building Blockchain-based Decentralized Applications - A Practical Guide. </b> </p>
                    <p className="lead" style={textStyle}> Ropsten Deployed Contract :
                        <b> <a href="https://ropsten.etherscan.io/address/0x15445dCe2a0B55C4f47291F8F06012B80e1A7dc4">0x15445dCe2a0B55C4f47291F8F06012B80e1A7dc4</a>  </b> </p>
                    <p className="lead" style={textStyle}> Other Resources:  </p>
                    <li style={textStyle} > <a href="https://gitlab.com/p.claudiadaniela/reactapp-metamask/-/tree/production">Project Source Code</a> </li>
                    <li style={textStyle} > <a href="https://dsrl.eu/claudia/">Book Source</a> </li>
                    <li  style={textStyle} > <a href="https://dsrl.eu/claudia/">Personal Page</a> </li>


                </Container>
            </Jumbotron>
        </div>
    );
}


export default Home;
