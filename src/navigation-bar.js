import React from 'react'
import  {
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    NavLink
} from 'reactstrap'


const textStyle = {
    color: 'black',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="light" light expand="md">
            <NavbarBrand href="/">
                <label>ETH Tutorial</label>
            </NavbarBrand>


            <Nav className="mr-auto" navbar>
                <NavItem>
                    <NavLink align="right" style={textStyle} href="/metamask-account">Metamask Account</NavLink>
                </NavItem>

                <NavItem>
                    <NavLink align="right" style={textStyle} href="/auction">Auctions </NavLink>
                </NavItem>

            </Nav>
        </Navbar>
    </div>

    );

export default NavigationBar
