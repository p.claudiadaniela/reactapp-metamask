# React App Demo using Metamask

This project has been developed according to the guide book :
### `Building Blockchain-based Decentralized Applications - A Practical Guide`

## Running the project
According to the tutorials presented in the book, make sure that you have started the OpenEthereum node.
In the project directory, you can run:


1. Compile the smart contract

```bash
truffle compile
```
2. Deploy the smart contract

```bash
truffle migrate --network parity
```
3. Set the address of the deployed contract in the web3.js file
4. Start the Application

```bash
npm start
```

## Setup OpenEthereum private network

You can run your own private network. Consider the instructions from the guide book and the configuration files found at https://gitlab.com/p.claudiadaniela/private-setup-openethereum 
